package main_test

import (
	"fmt"
	"nqueen/approach1"
	"nqueen/approach2"
	"reflect"
	"testing"
)

type NQueensTestCase struct {
	n        int
	expected [][]string
}

var testCases = []NQueensTestCase{
	{
		n: 1,
		expected: [][]string{
			{"Q"},
		},
	},
	{
		n:        2,
		expected: [][]string{},
	},
	{
		n:        3,
		expected: [][]string{},
	},
	{
		n: 4,
		expected: [][]string{
			{
				".Q..",
				"...Q",
				"Q...",
				"..Q.",
			},
			{
				"..Q.",
				"Q...",
				"...Q",
				".Q..",
			},
		},
	},
}

func TestSolveNQueensApproach1(t *testing.T) {
	for _, tc := range testCases {
		t.Run(fmt.Sprintf("NQueens Approach1 n = %d", tc.n), func(t *testing.T) {
			result := approach1.SolveNQueens(tc.n)
			if !reflect.DeepEqual(result, tc.expected) {
				t.Errorf("Test failed for n = %d \n Expected: %v \n Actual: %v \n", tc.n, tc.expected, result)
			}
		})
	}
}

func TestSolveNQueensApproach2(t *testing.T) {
	for _, tc := range testCases {
		t.Run(fmt.Sprintf("NQueens Approach2 n = %d", tc.n), func(t *testing.T) {
			result := approach2.SolveNQueens(tc.n)
			if !reflect.DeepEqual(result, tc.expected) {
				t.Errorf("Test failed for n = %d \n Expected: %v \n Actual: %v \n", tc.n, tc.expected, result)
			}
		})
	}
}
