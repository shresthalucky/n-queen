package main

import (
	"fmt"
	"nqueen/approach1"
	"nqueen/approach2"
	"time"
)

func main() {
	// n := 4
	// solutions := approach1.SolveNQueens(n)
	// for _, solution := range solutions {
	// 	for _, row := range solution {
	// 		fmt.Println(row)
	// 	}
	// 	fmt.Println()
	// }

	calculateTime()
}

func calculateTime() {
	for n := 1; n <= 8; n++ {
		solveWithApproach1(n)
		solveWithApproach2(n)
	}
}

func solveWithApproach1(n int) {
	defer timer(fmt.Sprintf("Approach 1 for n = %d ", n))()
	approach1.SolveNQueens(n)
}

func solveWithApproach2(n int) {
	defer timer(fmt.Sprintf("Approach 2 for n = %d ", n))()
	approach2.SolveNQueens(n)
}

func timer(name string) func() {
	start := time.Now()
	return func() {
		fmt.Printf("%s took %v\n", name, time.Since(start))
	}
}
