package approach1

const (
	queen, empty = 'Q', '.'
)

// solvenqueens returns all distinct solutions to the n-queens puzzle
func SolveNQueens(n int) [][]string {
	solutions := make([][]string, 0)
	board := make([][]byte, n)
	for i := range board {
		board[i] = make([]byte, n)
		for j := range board[i] {
			board[i][j] = empty
		}
	}

	var backtrack func(row int)
	backtrack = func(row int) {
		if row == n {
			// all rows are filled, a valid board configuration
			var solution []string
			for _, row := range board {
				solution = append(solution, string(row))
			}
			solutions = append(solutions, solution)
			return
		}

		for col := 0; col < n; col++ {
			if isSafe(board, row, col, n) {
				board[row][col] = queen // place the queen
				backtrack(row + 1)      // move to the next row
				board[row][col] = empty // remove the queen (backtrack)
			}
		}
	}

	backtrack(0)
	return solutions
}

// isSafe checks if it's safe to place a queen at board[row][col]
func isSafe(board [][]byte, row, col, n int) bool {
	// check column
	for i := 0; i < row; i++ {
		if board[i][col] == queen {
			return false
		}
	}

	// check major diagonal (\)
	for i, j := row, col; i >= 0 && j >= 0; i, j = i-1, j-1 {
		if board[i][j] == queen {
			return false
		}
	}

	// check minor diagonal (/)
	for i, j := row, col; i >= 0 && j < n; i, j = i-1, j+1 {
		if board[i][j] == queen {
			return false
		}
	}

	return true
}
