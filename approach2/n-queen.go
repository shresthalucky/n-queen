package approach2

// SolveNQueens returns all distinct solutions to the n-queens problem
func SolveNQueens(n int) [][]string {
	var queen, empty byte // Bytes to store Queen and empty sqaure character
	queen, empty = 'Q', '.'

	solutions := make([][]string, 0)    // Slice to store all valid board configurations
	cols := make([]bool, n, n)          // Slice to track which columns are occupied
	diag1 := make([]bool, 2*n-1, 2*n-1) // Slice to track which major diagonals (\) are occupied
	diag2 := make([]bool, 2*n-1, 2*n-1) // Slice to track which minor diagonals (/) are occupied
	queens := make([]int, n, n)         // Slice to store the column position of queens for each row

	// backtrack is a recursive function to place queens row by row
	var backtrack func(row int)
	backtrack = func(row int) {
		if row == n {
			// All rows are filled, so we have a valid board
			// Create a board with n rows
			board := make([]string, n)
			for i := range board {
				row := make([]byte, n)
				for j, _ := range row {
					row[j] = empty
				}
				row[queens[i]] = queen
				board[i] = string(row)
			}

			// Add board to solutions
			solutions = append(solutions, board)
			return
		}

		for col := 0; col < n; col++ {
			d1, d2 := row-col+n-1, row+col

			// Check if placing a queen at (row, col) is safe
			if cols[col] || diag1[d1] || diag2[d2] {
				continue // Not safe, skip this column
			}

			// Place the queen at (row, col)
			queens[row] = col
			cols[col], diag1[d1], diag2[d2] = true, true, true

			// Move to the next row
			backtrack(row + 1)

			// Remove the queen and backtrack
			cols[col], diag1[d1], diag2[d2] = false, false, false
		}
	}

	// Start backtracking from the first row
	backtrack(0)
	return solutions
}
